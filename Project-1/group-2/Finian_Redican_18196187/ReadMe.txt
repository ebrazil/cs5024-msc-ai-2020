Strategy
-----------
1) Process starts setting initial player score = 0.
2) Player selects a cell.
3a) If cell is a corner: score = score + 20.
3b) If cell is an edge: score = score + 10.
3c) If cell is neither a corner or edge, then it is classed as a middle cell with no update to score.
4) End process.

Implementation includes two process files:
---------------------------------------------------------
Finians_Updated_AI_process.process – Main process.
Finians_Updated_SubAI.process – sub process used to implement custom check for item 3a, 3b and 3c outlined above. 

Run instructions:
------------------------
1) After importing both files into a AI Project, select option G to generate Code
2) Under Player 2 select ‘Finians_Updated_AI_process’
3) If you have the latest version of Dime with the working debug option. Select the ‘Open Debug’ option
4) As you play you will see the Player 2 cell scores in the debug window updating to be 20 (corner) or 10 (edge) or unchanged if neither corner or edge cell.
